import React from 'react';
import {Formik,  Form, Field} from "formik";
import FormInput from '../../components/FormInput/FormInput';
import schema from './validationSchema'
import {useDispatch} from "react-redux";
import loginActions from '../../redux/Actions/userActions'
import "./landing.scss"
import { Link } from 'react-router-dom';
import Alert from '../../components/Alert/Alert';

const LoginPage = ({history}) => {
  const dispatch = useDispatch();
  const handleSubmit = async (values, {setSubmitting, resetForm}) => {
    dispatch(loginActions.loginAsync(values))
    resetForm()
    setSubmitting(false)
    // history.push('/')

  }


  return (
    <div className="login">
        <Alert/>
        <div className="form">
        <div className="form__inner">
            <div className="form__head">
                <h1 className="form__name">Login</h1>
            </div>
        <Formik
                    initialValues={{ email: '',  password: '' }}
                    validationSchema={schema.loginSchema}
                    onSubmit={handleSubmit}
                >
                    {({handleChange, onSubmit, isValid, dirty,  isSubmitting}) => (
                        <Form>
                            <label className="form__label" htmlFor="email">Email</label>
                            <Field
                                component={FormInput}
                                type="text"
                                name="email"
                                onChange={handleChange}
                            />
                            <label className="form__label" htmlFor="password">Password</label>
                            <Field
                                component={FormInput}
                                type="password"
                                name="password"
                                onChange={handleChange}
                            />
                            <button type="submit" disabled={!isValid || !dirty || isSubmitting}>
                                Login
                            </button>
                        </Form>
                    )}
                </Formik>
        </div>
        </div>

        <div className="link"> <Link to="/register">Register</Link></div>
       

    </div>
  );
};

export default LoginPage;