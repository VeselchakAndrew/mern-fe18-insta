import {MODAL_TOGGLE} from "../ActionTypes/userActionTypes";

const toggleModal = (data)=>({
    type: MODAL_TOGGLE,
    payload: data
});

export default {
    toggleModal
}