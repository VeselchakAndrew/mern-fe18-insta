import {SAVE_POSTS} from '../ActionTypes/userActionTypes';
import {getAllPosts, createPostPut, deletePostFetch} from "../../utils/FetchToServer";


const savePostFromUser = (data)=>({
    type: SAVE_POSTS,
    payload: data
});

const saveAllPostFromUserAsync = (currentPage) => async dispatch => {
      const data = await getAllPosts(currentPage);
      dispatch(savePostFromUser(data));
};


const updetePosts = (currentPage)=> async dispatch => {
    const data = await getAllPosts(currentPage);
    dispatch(savePostFromUser(data));
};

const createPostAsync = (data) => async dispatch => {
    await createPostPut(data);
    dispatch(updetePosts(1))
};

const deletePost = (id) => dispatch => {
    deletePostFetch(id)
};




export default {
    savePostFromUser,
    saveAllPostFromUserAsync,
    createPostAsync,
    deletePost
}