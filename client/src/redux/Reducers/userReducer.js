import {
    USER_LOGIN,
    USER_EXIT,
    ADD_LIKE_TO_POST,
    REMOVE_LIKE_FROM_POST,
    SUBSCRIBE_TO_USER,
    UNSUBSCRIBE_FROM_USER,
    SET_SUGGESTION
    
} from "../ActionTypes/userActionTypes";

const initialState = {
    id: "",
    name: "",
    avatar: "",
    firstName: "",
    lastName: "",
    email: "",
    followers: [],
    following: [],
    likedPosts: [],
    isAuth: false,
    suggestions: []
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOGIN:
            return {
                ...state,
                id: action.payload.id,
                name: action.payload.name,
                avatar: action.payload.photo,
                firstName: action.payload.firstName,
                lastName: action.payload.lastName,
                email: action.payload.email,
                followers: action.payload.followers,
                following: action.payload.following,
                isAuth: true
            };
        case USER_EXIT:
            return {
                ...state,
                isAuth: false
            };
        case ADD_LIKE_TO_POST:
            return {
                ...state,
                likedPosts: action.payload
            };
        case REMOVE_LIKE_FROM_POST:
            return {
                ...state,
                likedPosts: action.payload
            };
        case SUBSCRIBE_TO_USER:
            return {
                ...state,
                // following: action.payload
            };
        case UNSUBSCRIBE_FROM_USER:
            return {
                ...state,
                // following: action.payload
            };
        case SET_SUGGESTION:
            return {
                ...state,
                suggestions: action.payload
            };
        
        default:
            return state;
    }
};

export default userReducer;