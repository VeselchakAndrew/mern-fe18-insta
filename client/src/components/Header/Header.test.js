import Header from "./Header";
import createMockStore from 'redux-mock-store';
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import {fireEvent} from "@testing-library/react";


jest.mock('../../svgIcons/Icons', ()=>()=><p>img</p>);
jest.mock("../../redux/Actions/modalAction", () => ({
    modal: ()=> false,
}));

const store = createMockStore([thunk])({});

test('Header render', ()=>{
    const {getByText, getByTestId} = render(<Provider store={store}><Header/></Provider>);

    expect(getByText('img').textContent).toBeDefined();
    expect(getByTestId('bth-create-post')).toBeDefined();
});

test('сlick on create post bth', ()=>{
    const clickBthMock = jest.fn();
    const {getByTestId} = render(<Provider store={store}><Header handleClickOnCreatePostTest={clickBthMock}/></Provider>);
    const button = getByTestId('bth-create-post');

    expect(button).toBeDefined();

    fireEvent.click(button);
    expect(clickBthMock).toBeCalled();

});

