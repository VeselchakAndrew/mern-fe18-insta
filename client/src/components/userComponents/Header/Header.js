import {useState} from "react";
import "./Header.scss";
import {followUser} from "../../../utils/FetchToServer";
import {useSelector} from "react-redux";

const Header = ({user, postCount, updateInfo}) => {
    const {_id, name, photo, firstName, lastName, followers, following} = user;

    const currentUser = useSelector(state => state.user);
    const followIndex = followers.find((el) => el === currentUser.id);
    const [follow, setFollow] = useState(!!followIndex);


    const handleClickFollowButton = async () => {
        setFollow(!follow);
        if (!follow) {
            await followUser(true, {followId: _id});
        } else {
            await followUser(false, {followId: _id});
        }
        await updateInfo(false);

    };


    return (
        <div className="header__container">
            <div className="header__user">
                <img src={photo} alt={name} className="header__user_avatar"/>
                <h3 className="header__nickname">{name}</h3>
            </div>

            <div className="header__user_info">
                <p className="header__user_name">{firstName} {lastName}</p>
                <p>Всего постов: {postCount}</p>
                <p>Followers: {followers.length}</p>
                <p>Following: {following.length}</p>
            </div>
            <button className="header__btn" onClick={() => handleClickFollowButton()}>{!follow ? "Subscribe" : "Unsubscribe"}</button>
        </div>
    );
};

export default Header;
