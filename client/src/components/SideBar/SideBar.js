import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import userActions from '../../redux/Actions/userActions';
import UserList from '../UserList/UserList';

const SideBar = () => {
  const following = useSelector(state=> state.user.following);
  const suggestion = useSelector(state=> state.user.suggestions)
  const dispatch = useDispatch()

  useEffect(()=> {
    dispatch(userActions.getSuggestionsAsync())
  }, [])

  return (
    <div className="bar">
        <div className="bar__item">
            <UserList users={following}  canSub={false} barTitle={"Following"} />
        </div>
        <div className="bar__item">
            <UserList users={suggestion}  canSub={true} barTitle={"Suggestions"} />
        </div>
    </div>
  );
};

export default SideBar;