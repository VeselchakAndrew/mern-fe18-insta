import postActions from "../redux/Actions/postActions";

// const url = "http://localhost:5000/api/v1";

const token = JSON.parse(localStorage.getItem("token"));

export const getAllPosts = (currentPage) => {
    return fetch(`/posts`, {
        headers: {
            Authorization: "Bearer " + token
        }
    })
        .then(res => {
            return res.json();
        })
        .then(data => {
            return data.data.filter((el, index) => index < currentPage * 3);
        });
};

export const likePost = (like, data) => {
    return fetch(`/posts/${like ? "unlike" : "like"}`, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
        },
        body: JSON.stringify(data),
    });
};

export const updatePost = (id, data) => {
    return fetch(`/posts/:${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json());

};

export const createPostPut = (data) => {
    return fetch(`/posts/craete`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json()).then(data => {
            return data.data;
        });
};


export const loginUser = async (credentials) => {
    return fetch("/auth/login", {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(credentials)
    })
        .then(res => res.json());
};

export const registerUser = async (credentials) => {
    return fetch(`/auth/register`, {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(credentials)
    })
        .then(res => res.json());
};

export const getSuggestions = async () => {
    return fetch(`/user/suggestions`, {
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
        },
    })
        .then(res => res.json());
};

export const deletePostFetch = (id) => {
    return fetch(`/posts/:${id}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
        },
    }).then(res => res.json()).then(console.log);
};

export const commentPostFetch = (data) => {
    return fetch(`/posts/comment`, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
        },
        body: JSON.stringify(data),
    }).then(console.log);
};


export const patchFollow = (data) => {
    return fetch(`/user/follow`, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
        },
        body: JSON.stringify(data),
    })
        .then(res => res.json());
};

export const patchUnFollow = (data) => {
    return fetch(`/user/unfollow`, {});

};
export const getAllUserPost = async (id) => {
    try {
        const data = await fetch(`/user/${id}`, {
            headers: {
                Authorization: "Bearer " + token
            }
        });
        return await data.json();
    } catch (e) {
        console.error(e);
    }
};
export const getSinglePost = async (id) => {
    try {
        const data = await fetch(`/posts/single/${id}`, {
            headers: {
                Authorization: "Bearer " + token
            }
        });
        return await data.json();
    } catch (e) {
        console.error(e);
    }
};

export const followUser = (follow, followId) => {
    try {
        return fetch(`/user/${follow ? "follow" : "unfollow"}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token
            },
            body: JSON.stringify(followId),
        });

    } catch (e) {
        console.error(e);
    }

};
