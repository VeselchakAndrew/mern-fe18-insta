import mongoose from 'mongoose'

export async function connectToMongoDb({config}){
  const connection = await mongoose
  .connect(
    // config.MONGODB_URI,
      "mongodb+srv://dbAdmin:db12345@insta-cluster.wq3qs.mongodb.net/insta-mern?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    }
  )
  return connection.connection.db;
}