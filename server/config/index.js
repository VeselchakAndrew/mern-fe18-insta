import dotenv from 'dotenv';

const enviroment = dotenv.config();

if (!enviroment) {
  throw new Error('Config file was not found');
}

export default {
  // PORT: process.env.PORT || 3005,
  SERVICE_PREFIX: process.env.SERVICE_PREFIX || '/api',
  VERSION: process.env.VERSION || 1,
  MONGODB_URI: process.env.MONGODB_URI || null,
  JWT_SECRET: process.env.JWT_SECRET || 'JWT_SECRET=ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDKNB1kFlf8DV/+ywpOM9RvE/hmaR67yShecW+R5jDNCDdiuN4tiidSTyKBwo2VAoL8RiIVtvMRM8ax+Z8JTF54dGsgwX+P4p2QO62u7WDBHohk2Jr7VuLtKeh22FQaVoeSef6namy11hexm5DaQ6eYgrI9pKAZRc0BUSQlpqRgtkN6QECq3SGdlS2BQ6bgK7kHyVd+b4PXLee8+4SbUJaJYMKPe8oTPfvmcomTgcSEFaRPHxScAyb9lqPP9C6OUxm4zV5eC/4PAlfOdWsIWRtqg3A80x1OyYbeGXmORVBF9M30cSfIRyck3EtBWW0DwFjmU3D7Ng9lIVxGqNngEnJY4LTPHDMlhzvN6eyUztmJZfatKXSF/46+0KoXBM8dzI6GWz1K3khYGW59Cf3KMz3s7XI87Dv2XzTTAY6RbrM9TORE0OHLA6/qZ5CMQpZa6tskI+dj/ivwIg7PWbAuJAA5FqazmCkojganYQheGn5mDf0aLlFUY6dwPqdvmd1x//0= oRlex@DESKTOP-TB9ECDJ',
  CLOUD_NAME: process.env.CLOUD_NAME || '',
  CLOUD_API_KEY: process.env.CLOUD_API_KEY || '',
  CLOUD_API_SECRET: process.env.CLOUD_API_SECRET || '',
}