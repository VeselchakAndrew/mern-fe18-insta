"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _config = _interopRequireDefault(require("../config"));

var _cloudinary = _interopRequireDefault(require("cloudinary"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_cloudinary["default"].v2.config({
  cloud_name: _config["default"].CLOUD_NAME,
  api_key: _config["default"].CLOUD_API_KEY,
  api_secret: _config["default"].CLOUD_API_SECRET
});

var _default = _cloudinary["default"];
exports["default"] = _default;
//# sourceMappingURL=cloudinary.js.map