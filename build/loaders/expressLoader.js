"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = _default;

var _api = _interopRequireDefault(require("../api"));

var _cors = _interopRequireDefault(require("cors"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _StatusService = _interopRequireDefault(require("../api/services/StatusService"));

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _cors2 = _interopRequireDefault(require("../middleware/cors"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// const allowedOrigins = [
//     "http://localhost:3000",
//     "https://fe18-react-step.herokuapp.com/"
// ];

/**
 * @desc Express loader
 **/
function _default(_ref) {
  var config = _ref.config,
      app = _ref.app;
  app.use(_express["default"]["static"](__dirname + "/public/"));
  app.use(_express["default"].json({
    limit: "50mb"
  }));
  app.use(_express["default"].urlencoded({
    limit: "50mb",
    extended: true
  }));
  app.use(_cors2["default"]);

  if (process.env.NODE_ENV === "production") {
    app.use(_express["default"]["static"]("client/build"));
    app.get("*", function (req, res) {
      res.sendFile(_path["default"].resolve(__dirname, "../client", "build", "index.html"));
    });
  } // app.use(cors({
  //     origin: function (origin, callback) {
  //         // allow requests with no origin
  //         // (like mobile apps or curl requests)
  //         if (!origin) {
  //             return callback(null, true);
  //         }
  //
  //         if (allowedOrigins.indexOf(origin) === -1) {
  //             const msg = "The CORS policy for this site does not allow access from the specified Origin.";
  //
  //             return callback(new Error(msg), false);
  //         }
  //
  //         return callback(null, true);
  //     },
  //     credentials: true
  // }));


  app.use(_bodyParser["default"].json()); // Load API routes

  app.use("".concat(config.SERVICE_PREFIX, "/v").concat(config.VERSION), (0, _api["default"])()); /// catch 404 and forward to error handler

  app.use(function (req, res, next) {
    var err = new Error("Not Found");
    err["status"] = 404;
    next(err);
  }); /// error handlers

  app.use(function (err, request, response, next) {
    var statusCode = _typeof(err) === "object" && err.status > 0 ? err.status : 500;
    var _err$status = err.status,
        status = _err$status === void 0 ? statusCode : _err$status,
        _err$name = err.name,
        name = _err$name === void 0 ? "" : _err$name,
        _err$message = err.message,
        error = _err$message === void 0 ? "Internal Application Error" : _err$message;
    var customResponse = response.status(statusCode).json(_StatusService["default"].buildError(error, statusCode));
    return name === "UnauthorizedError" ? customResponse.end() : customResponse;
  });
}
//# sourceMappingURL=expressLoader.js.map