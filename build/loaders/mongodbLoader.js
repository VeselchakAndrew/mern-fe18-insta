"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.connectToMongoDb = connectToMongoDb;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function connectToMongoDb(_x) {
  return _connectToMongoDb.apply(this, arguments);
}

function _connectToMongoDb() {
  _connectToMongoDb = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref) {
    var config, connection;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            config = _ref.config;
            _context.next = 3;
            return _mongoose["default"].connect( // config.MONGODB_URI,
            "mongodb+srv://dbAdmin:db12345@insta-cluster.wq3qs.mongodb.net/insta-mern?retryWrites=true&w=majority", {
              useNewUrlParser: true,
              useCreateIndex: true,
              useUnifiedTopology: true,
              useFindAndModify: false
            });

          case 3:
            connection = _context.sent;
            return _context.abrupt("return", connection.connection.db);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _connectToMongoDb.apply(this, arguments);
}
//# sourceMappingURL=mongodbLoader.js.map