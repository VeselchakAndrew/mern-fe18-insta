"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ObjectId = _mongoose["default"].Schema.Types.ObjectId;
var Post = new _mongoose["default"].Schema({
  title: {
    type: String,
    required: true
  },
  body: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    "default": "no photo"
  },
  postedBy: {
    type: ObjectId,
    ref: "User"
  },
  createdAt: {
    type: Date,
    "default": new Date()
  },
  updatedAt: {
    type: Date,
    "default": new Date()
  },
  likes: [{
    type: ObjectId,
    ref: "User"
  }],
  comments: [{
    text: String,
    postedBy: {
      type: ObjectId,
      ref: "User"
    }
  }]
});

var _default = _mongoose["default"].model('Post', Post);

exports["default"] = _default;
//# sourceMappingURL=post.model.js.map