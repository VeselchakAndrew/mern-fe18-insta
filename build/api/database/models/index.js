"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "PostModel", {
  enumerable: true,
  get: function get() {
    return _postModel["default"];
  }
});
Object.defineProperty(exports, "UserModel", {
  enumerable: true,
  get: function get() {
    return _userModel["default"];
  }
});

var _postModel = _interopRequireDefault(require("./post.model.js"));

var _userModel = _interopRequireDefault(require("./user.model.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
//# sourceMappingURL=index.js.map