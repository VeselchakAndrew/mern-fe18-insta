"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _auth = _interopRequireDefault(require("./routes/auth"));

var _posts = _interopRequireDefault(require("./routes/posts"));

var _user = _interopRequireDefault(require("./routes/user"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default() {
  var router = (0, _express.Router)();
  (0, _auth["default"])(router);
  (0, _posts["default"])(router);
  (0, _user["default"])(router);
  return router;
};

exports["default"] = _default;
//# sourceMappingURL=index.js.map