"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UnlikeController = exports.CommentController = exports.LikePostController = exports.DeletePostController = exports.UpdatePostController = exports.SinglePostController = exports.CreatePostController = exports.AllPostsController = exports.SubPostsController = void 0;

var _mappers = require("../../mappers");

var _PostService = _interopRequireDefault(require("../../services/PostService"));

var _StatusService = _interopRequireDefault(require("../../services/StatusService"));

var _cloudinary = _interopRequireDefault(require("../../../utils/cloudinary"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var SubPostsController = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(request, response) {
    var following, result;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            following = request.body.following;
            _context.next = 4;
            return new _PostService["default"]().getSubPosts({
              following: following
            });

          case 4:
            result = _context.sent;
            response.status(200).json(_StatusService["default"].buildResponse(true, Array.isArray(result) ? result.map(function (item) {
              return _mappers.PostMapper.purePost(item);
            }) : []));
            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](0);
            response.json(_StatusService["default"].buildError(_context.t0.message, _context.t0.status));

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 8]]);
  }));

  return function SubPostsController(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.SubPostsController = SubPostsController;

var AllPostsController = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(request, response) {
    var result;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return new _PostService["default"]().getAllPosts();

          case 3:
            result = _context2.sent;
            response.status(200).json(_StatusService["default"].buildResponse(true, Array.isArray(result) ? result.map(function (item) {
              return _mappers.PostMapper.purePost(item);
            }) : []));
            _context2.next = 10;
            break;

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2["catch"](0);
            response.json(_StatusService["default"].buildError(_context2.t0.message, _context2.t0.status));

          case 10:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 7]]);
  }));

  return function AllPostsController(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

exports.AllPostsController = AllPostsController;

var CreatePostController = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(request, response) {
    var _request$body, title, body, photo, id, _yield$cloudinary$upl, secure_url, createdPost, post;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _request$body = request.body, title = _request$body.title, body = _request$body.body, photo = _request$body.photo;
            id = request.user.id;

            if (!(!title || !body || !photo)) {
              _context3.next = 5;
              break;
            }

            throw new Error("Please submit all the required fields");

          case 5:
            _context3.next = 7;
            return _cloudinary["default"].uploader.upload(photo);

          case 7:
            _yield$cloudinary$upl = _context3.sent;
            secure_url = _yield$cloudinary$upl.secure_url;
            _context3.next = 11;
            return new _PostService["default"]().createPost(title, body, secure_url, id);

          case 11:
            createdPost = _context3.sent;
            _context3.next = 14;
            return new _PostService["default"]().getSinglePost(createdPost.id);

          case 14:
            post = _context3.sent;
            response.status(201).json(_StatusService["default"].buildResponse(true, _mappers.PostMapper.purePost(post)));
            _context3.next = 21;
            break;

          case 18:
            _context3.prev = 18;
            _context3.t0 = _context3["catch"](0);
            response.json(_StatusService["default"].buildError(_context3.t0.message, _context3.t0.status));

          case 21:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 18]]);
  }));

  return function CreatePostController(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();

exports.CreatePostController = CreatePostController;

var SinglePostController = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(request, response) {
    var postId, post;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            postId = request.params.postId;

            if (postId) {
              _context4.next = 4;
              break;
            }

            throw new Error("Expected an ID but the parameter is undefined");

          case 4:
            _context4.next = 6;
            return new _PostService["default"]().getSinglePost(postId);

          case 6:
            post = _context4.sent;
            response.status(200).json(_StatusService["default"].buildResponse(true, post));
            _context4.next = 13;
            break;

          case 10:
            _context4.prev = 10;
            _context4.t0 = _context4["catch"](0);
            response.json(_StatusService["default"].buildError(_context4.t0.message, _context4.t0.status));

          case 13:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 10]]);
  }));

  return function SinglePostController(_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}();

exports.SinglePostController = SinglePostController;

var UpdatePostController = function UpdatePostController(request, response) {
  response.status(201).json({
    status: true
  });
};

exports.UpdatePostController = UpdatePostController;

var DeletePostController = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(request, response) {
    var _request$params, postId, user, result;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _request$params = request.params, postId = _request$params.postId, user = _request$params.user;
            _context5.prev = 1;
            _context5.next = 4;
            return new _PostService["default"]().deletePost({
              postId: postId,
              user: user
            });

          case 4:
            result = _context5.sent;
            response.status(201).json(_StatusService["default"].buildResponse(true, result));
            _context5.next = 11;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](1);
            response.json(_StatusService["default"].buildError(_context5.t0.message, _context5.t0.status));

          case 11:
            response.status(201).json({
              status: true
            });

          case 12:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[1, 8]]);
  }));

  return function DeletePostController(_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}();

exports.DeletePostController = DeletePostController;

var LikePostController = /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(request, response) {
    var postId, id, result;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            postId = request.body.postId;
            id = request.user.id;
            _context6.prev = 2;
            _context6.next = 5;
            return new _PostService["default"]().likePost(postId, id);

          case 5:
            result = _context6.sent;
            response.status(201).json(_StatusService["default"].buildResponse(true, _mappers.PostMapper.purePost(result)));
            _context6.next = 12;
            break;

          case 9:
            _context6.prev = 9;
            _context6.t0 = _context6["catch"](2);
            response.json(_StatusService["default"].buildError(_context6.t0.message, _context6.t0.status));

          case 12:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[2, 9]]);
  }));

  return function LikePostController(_x11, _x12) {
    return _ref6.apply(this, arguments);
  };
}();

exports.LikePostController = LikePostController;

var CommentController = /*#__PURE__*/function () {
  var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(request, response) {
    var _request$body2, postId, text, id, result;

    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _request$body2 = request.body, postId = _request$body2.postId, text = _request$body2.text;
            id = request.user.id;
            _context7.prev = 2;
            _context7.next = 5;
            return new _PostService["default"]().commentPost(postId, text, id);

          case 5:
            result = _context7.sent;
            response.status(201).json(_StatusService["default"].buildResponse(true, _mappers.PostMapper.purePost(result)));
            _context7.next = 12;
            break;

          case 9:
            _context7.prev = 9;
            _context7.t0 = _context7["catch"](2);
            response.json(_StatusService["default"].buildError(_context7.t0.message, _context7.t0.status));

          case 12:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[2, 9]]);
  }));

  return function CommentController(_x13, _x14) {
    return _ref7.apply(this, arguments);
  };
}();

exports.CommentController = CommentController;

var UnlikeController = /*#__PURE__*/function () {
  var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(request, response) {
    var postId, id, result;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            postId = request.body.postId;
            id = request.user.id;
            _context8.prev = 2;
            _context8.next = 5;
            return new _PostService["default"]().unlikePost(postId, id);

          case 5:
            result = _context8.sent;
            response.status(201).json(_StatusService["default"].buildResponse(true, _mappers.PostMapper.purePost(result)));
            _context8.next = 12;
            break;

          case 9:
            _context8.prev = 9;
            _context8.t0 = _context8["catch"](2);
            response.json(_StatusService["default"].buildError(_context8.t0.message, _context8.t0.status));

          case 12:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[2, 9]]);
  }));

  return function UnlikeController(_x15, _x16) {
    return _ref8.apply(this, arguments);
  };
}();

exports.UnlikeController = UnlikeController;
//# sourceMappingURL=controller.js.map