"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _controller = require("./controller");

var _middleware = _interopRequireDefault(require("../../middleware"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var route = (0, _express.Router)();

var _default = function _default(mainRoute) {
  mainRoute.use('/posts', route);
  /**
   * @route   GET  /posts/
   * @desc    Return all posts
   * @access  Private
   */

  route.get('/', _middleware["default"], _controller.AllPostsController);
  /**
   * @route   GET  /posts/subposts
   * @desc    Return all posts
   * @access  Private
   * 
   */

  route.get('/subposts', _middleware["default"], _controller.SubPostsController);
  /**
  * @route   GET  /posts/:postId
  * @desc    Post id / Return Single post
  * @access  Private
  * @param {id}
  * 
  */

  route.get('/single/:postId', _middleware["default"], _controller.SinglePostController);
  /**
   * @route   GET  /posts/single/:postID
   * @desc    Return  post
   * @access  Private
   * 
   */

  route.get('/:postID', _middleware["default"], _controller.SinglePostController);
  /**
   * @route   PATCH  /posts/comment
   * @desc    Return  post
   * @access  Private
   * 
   */

  route.patch('/comment', _middleware["default"], _controller.CommentController);
  /**
   * @route   POST  /posts/craete
   * @desc    Post body / Return created post
   * @access  Private
   * @param {title}  
   * @param {body}
   * @param {photo}
   * 
   */

  route.post('/craete', _middleware["default"], _controller.CreatePostController);
  /**
   * @route   PUT  /posts/:postId
   * @desc    Post body / Return updated post
   * @access  Private
   * @ignore
  */

  route.put('/:postId', _middleware["default"], _controller.UpdatePostController);
  /**
   * @route   DELETE  /posts/:postId
   * @desc    Delete selected post
   * @access  Private
   * @ignore
   *  
  */

  route["delete"]('/:postId', _middleware["default"], _controller.DeletePostController);
  /** 
   * @route   PUT  /posts/like
   * @desc    Like selected post
   * @access  Private
   * @param {postId} Post ID
   * @param {userId} User ID
  */

  route.patch('/like', _middleware["default"], _controller.LikePostController);
  /**
   * @description Unlike selected post
   * @rote PUT  /posts/unlike
   * @access Private
   * @param {postId} Post ID
   * @param {userId} User ID
  */

  route.patch('/unlike', _middleware["default"], _controller.UnlikeController);
};

exports["default"] = _default;
//# sourceMappingURL=index.js.map