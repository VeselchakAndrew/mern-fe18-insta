"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SuggestionController = exports.UnfollowController = exports.FollowController = exports.SingleUserController = void 0;

var _express = require("express");

var _mappers = require("../../mappers");

var _UserService = _interopRequireDefault(require("../../services/UserService"));

var _StatusService = _interopRequireDefault(require("../../services/StatusService"));

var _mongoose = require("mongoose");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var SingleUserController = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(request, response) {
    var userID, result, user, posts;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            userID = request.params.userID;
            _context.prev = 1;
            _context.next = 4;
            return new _UserService["default"]().getSinglePost(userID);

          case 4:
            result = _context.sent;
            user = result.user, posts = result.posts;
            response.status(200).json(_StatusService["default"].buildResponse(true, {
              user: user,
              posts: Array.isArray(posts) ? posts.map(function (item) {
                return _mappers.PostMapper.purePost(item);
              }) : []
            }));
            _context.next = 12;
            break;

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](1);
            response.json(_StatusService["default"].buildError(_context.t0.message, _context.t0.status));

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 9]]);
  }));

  return function SingleUserController(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.SingleUserController = SingleUserController;

var FollowController = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(request, response) {
    var followId, id, result;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            followId = request.body.followId;
            id = request.user.id;

            if (!(!followId || !id)) {
              _context2.next = 5;
              break;
            }

            throw new _mongoose.Error("followId or id is undefined");

          case 5:
            _context2.next = 7;
            return new _UserService["default"]().follow(followId, id);

          case 7:
            result = _context2.sent;
            response.status(201).json(_StatusService["default"].buildResponse(true, result));
            _context2.next = 14;
            break;

          case 11:
            _context2.prev = 11;
            _context2.t0 = _context2["catch"](0);
            response.json(_StatusService["default"].buildError(_context2.t0.message, _context2.t0.status));

          case 14:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 11]]);
  }));

  return function FollowController(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

exports.FollowController = FollowController;

var UnfollowController = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(request, response) {
    var followId, id, result;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            followId = request.body.followId;
            id = request.user.id;

            if (!(!followId || !id)) {
              _context3.next = 5;
              break;
            }

            throw new _mongoose.Error("followId or id is undefined");

          case 5:
            _context3.next = 7;
            return new _UserService["default"]().unFollow(followId, id);

          case 7:
            result = _context3.sent;
            response.status(201).json(_StatusService["default"].buildResponse(true, result));
            _context3.next = 14;
            break;

          case 11:
            _context3.prev = 11;
            _context3.t0 = _context3["catch"](0);
            response.json(_StatusService["default"].buildError(_context3.t0.message, _context3.t0.status));

          case 14:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 11]]);
  }));

  return function UnfollowController(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();

exports.UnfollowController = UnfollowController;

var SuggestionController = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(request, response) {
    var id, result;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            id = request.user.id;
            _context4.next = 4;
            return new _UserService["default"]().getSuggestion(id);

          case 4:
            result = _context4.sent;
            response.status(200).json(_StatusService["default"].buildResponse(true, result));
            _context4.next = 11;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](0);
            response.json(_StatusService["default"].buildError(_context4.t0.message, _context4.t0.status));

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 8]]);
  }));

  return function SuggestionController(_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}();

exports.SuggestionController = SuggestionController;
//# sourceMappingURL=controller.js.map