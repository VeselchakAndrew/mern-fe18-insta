"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _controller = require("./controller");

var _middleware = _interopRequireDefault(require("../../middleware"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var route = (0, _express.Router)();

var _default = function _default(mainRoute) {
  // Prefix  /user
  mainRoute.use('/user', route);
  /**
   * @route   GET  /user/:userID
   * @desc    Return single User
   * @access  Private
   */

  route.get('/suggestions/', _middleware["default"], _controller.SuggestionController);
  /**
   * @route   GET  /user/:userID
   * @desc    Return single User
   * @access  Private
   */

  route.get('/:userID/', _middleware["default"], _controller.SingleUserController);
  /**
  * @route   patch  /user/follow
  * @desc    Follow the user / 
  * @access  Private
  */

  route.patch('/follow/', _middleware["default"], _controller.FollowController);
  /**
  * @route   patch  /user/unfollow
  * @desc    Follow the user / 
  * @access  Private
  */

  route.patch('/unfollow/', _middleware["default"], _controller.UnfollowController);
};

exports["default"] = _default;
//# sourceMappingURL=index.js.map