"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _middleware = _interopRequireDefault(require("../../middleware"));

var _controller = require("./controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var route = (0, _express.Router)();

var _default = function _default(mainRoute) {
  // Prefix  /auth
  mainRoute.use('/auth', route); // @route   POST  /auth/register
  // @desc    Register User / Return JWT Token
  // @access  Public

  route.post('/register', _controller.RegisterController); // @route   GET  /auth/login
  // @desc    Login User / Return JWT Token
  // @access  Public

  route.post('/login', _controller.LoginController); // @route   GET  /auth/login
  // @desc    Login User / Return JWT Token
  // @access  Public

  route.get('/getuser', _middleware["default"], _controller.GetUserController);
  /**
   * @route GET /auth/user
   * @desc Get logged User
   * @acces Private
   */

  route.get('/user', _middleware["default"], _controller.LoggedUserController);
};

exports["default"] = _default;
//# sourceMappingURL=index.js.map