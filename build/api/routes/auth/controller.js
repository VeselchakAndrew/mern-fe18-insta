"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LoggedUserController = exports.GetUserController = exports.LoginController = exports.RegisterController = void 0;

var _mappers = require("../../mappers");

var _AuthService = _interopRequireDefault(require("../../services/AuthService"));

var _models = require("../../database/models");

var _StatusService = _interopRequireDefault(require("../../services/StatusService"));

var _cloudinary = _interopRequireDefault(require("../../../utils/cloudinary"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var RegisterController = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(request, response) {
    var _request$body, name, email, firstName, lastName, password, photo, uploadedResponse, _yield$AuthService$re, user, token;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _request$body = request.body, name = _request$body.name, email = _request$body.email, firstName = _request$body.firstName, lastName = _request$body.lastName, password = _request$body.password, photo = _request$body.photo;
            _context.prev = 1;
            _context.next = 4;
            return _models.UserModel.findOne({
              email: email
            });

          case 4:
            if (!_context.sent) {
              _context.next = 6;
              break;
            }

            throw new Error("Already exist");

          case 6:
            _context.next = 8;
            return _cloudinary["default"].uploader.upload(photo);

          case 8:
            uploadedResponse = _context.sent;
            _context.next = 11;
            return new _AuthService["default"]().registerUser(name, email, firstName, lastName, password, uploadedResponse);

          case 11:
            _yield$AuthService$re = _context.sent;
            user = _yield$AuthService$re.user;
            token = _yield$AuthService$re.token;
            response.status(201).json(_StatusService["default"].buildResponse(true, {
              user: _mappers.UserMapper.createdUser(user),
              token: token
            }));
            _context.next = 20;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](1);
            response.json(_StatusService["default"].buildError(_context.t0.message, _context.t0.status));

          case 20:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 17]]);
  }));

  return function RegisterController(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.RegisterController = RegisterController;

var LoginController = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(request, response) {
    var _request$body2, email, password, _yield$AuthService$lo, user, token, error;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _request$body2 = request.body, email = _request$body2.email, password = _request$body2.password;
            _context2.prev = 1;

            if (!(!email || !password)) {
              _context2.next = 4;
              break;
            }

            throw new Error("email or password in undefined");

          case 4:
            _context2.next = 6;
            return new _AuthService["default"]().loginUser({
              email: email,
              password: password
            });

          case 6:
            _yield$AuthService$lo = _context2.sent;
            user = _yield$AuthService$lo.user;
            token = _yield$AuthService$lo.token;
            error = _yield$AuthService$lo.error;

            if (!error) {
              _context2.next = 12;
              break;
            }

            throw new Error(error);

          case 12:
            response.status(201).json(_StatusService["default"].buildResponse(true, {
              user: _mappers.UserMapper.loginUser(user),
              token: token
            }));
            _context2.next = 18;
            break;

          case 15:
            _context2.prev = 15;
            _context2.t0 = _context2["catch"](1);
            response.json(_StatusService["default"].buildError(_context2.t0.message, _context2.t0.status));

          case 18:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[1, 15]]);
  }));

  return function LoginController(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

exports.LoginController = LoginController;

var GetUserController = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(request, response) {
    var email, user;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            email = request.user.email;
            _context3.prev = 1;

            if (email) {
              _context3.next = 4;
              break;
            }

            throw new Error("email or password in undefined");

          case 4:
            _context3.next = 6;
            return new _AuthService["default"]().getUser(email);

          case 6:
            user = _context3.sent;
            console.log(user);
            response.status(200).json(_StatusService["default"].buildResponse(true, {
              user: user
            }));
            _context3.next = 14;
            break;

          case 11:
            _context3.prev = 11;
            _context3.t0 = _context3["catch"](1);
            response.json(_StatusService["default"].buildError(_context3.t0.message, _context3.t0.status));

          case 14:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[1, 11]]);
  }));

  return function GetUserController(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();

exports.GetUserController = GetUserController;

var LoggedUserController = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(request, response) {
    var token;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            token = request.user.token;

          case 1:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function LoggedUserController(_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}();

exports.LoggedUserController = LoggedUserController;
//# sourceMappingURL=controller.js.map