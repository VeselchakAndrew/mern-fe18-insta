"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = _default;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _config = _interopRequireDefault(require("../../config"));

var _models = require("../database/models");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _default(request, response, next) {
  var authorization = request.headers.authorization;

  if (!authorization) {
    return response.status(401).json({
      error: "You must be logged In"
    });
  }

  var token = authorization.replace("Bearer ", "");

  _jsonwebtoken["default"].verify(token, _config["default"].JWT_SECRET, function (err, payload) {
    if (err) {
      return response.status(401).json({
        error: "You must be logged In"
      });
    }

    var id = payload.id;

    _models.UserModel.findById(id).then(function (userdata) {
      // We make user data accessible
      request.user = userdata;
      next();
    });
  });
}
//# sourceMappingURL=index.js.map