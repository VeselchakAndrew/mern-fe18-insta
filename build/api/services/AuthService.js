"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _models = require("../database/models");

var _gravatar = _interopRequireDefault(require("gravatar"));

var _crypto = require("crypto");

var _argon = _interopRequireDefault(require("argon2"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _config = _interopRequireDefault(require("../../config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AuthService = /*#__PURE__*/function () {
  function AuthService() {
    _classCallCheck(this, AuthService);
  }

  _createClass(AuthService, [{
    key: "loginUser",

    /**
     * @description LOGIN USER / RETURN USER AND TOKEN
     */
    value: function () {
      var _loginUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref) {
        var email, password, user;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                email = _ref.email, password = _ref.password;
                _context.next = 3;
                return _models.UserModel.findOne({
                  email: email
                }).populate("following", "id name photo").populate("followers", "id name photo");

              case 3:
                user = _context.sent;

                if (user) {
                  _context.next = 6;
                  break;
                }

                throw new Error("Cant find user");

              case 6:
                _context.next = 8;
                return _argon["default"].verify(user.password, password);

              case 8:
                if (!_context.sent) {
                  _context.next = 12;
                  break;
                }

                return _context.abrupt("return", {
                  user: user,
                  token: this.generateToken(user._id)
                });

              case 12:
                throw new Error("password did not match");

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function loginUser(_x) {
        return _loginUser.apply(this, arguments);
      }

      return loginUser;
    }()
    /**
     * @description GET USER / RETURN USER
     */

  }, {
    key: "getUser",
    value: function () {
      var _getUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(email) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _models.UserModel.findOne({
                  email: email
                }).populate("following", "id name photo").populate("followers", "id name photo");

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getUser(_x2) {
        return _getUser.apply(this, arguments);
      }

      return getUser;
    }()
    /**
     * @description Register a new user. Create item in UserCollection and save a picture
     */

  }, {
    key: "registerUser",
    value: function () {
      var _registerUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(name, email, firstName, lastName, password, photo) {
        var salt, pswd, avatar, user;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                salt = (0, _crypto.randomBytes)(12);
                _context3.next = 3;
                return _argon["default"].hash(password, {
                  salt: salt
                });

              case 3:
                pswd = _context3.sent;
                avatar = '';

                if (!photo) {
                  avatar = _gravatar["default"].url(email, {
                    s: '200',
                    // Size
                    r: 'pg',
                    // Rating
                    d: 'mm' // Default

                  });
                } else {
                  avatar = photo.secure_url;
                }

                _context3.next = 8;
                return _models.UserModel.create({
                  name: name,
                  email: email,
                  firstName: firstName,
                  lastName: lastName,
                  password: pswd,
                  photo: avatar
                });

              case 8:
                user = _context3.sent;
                return _context3.abrupt("return", {
                  user: user,
                  token: this.generateToken(user._id)
                });

              case 10:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function registerUser(_x3, _x4, _x5, _x6, _x7, _x8) {
        return _registerUser.apply(this, arguments);
      }

      return registerUser;
    }()
  }, {
    key: "generateToken",

    /**
     * @description Generate JWT TOKEN
     * @param {string} userID 
     */
    value: function generateToken(userId) {
      var today = new Date();
      var exp = new Date(today);
      exp.setDate(today.getDate() + 60);
      return _jsonwebtoken["default"].sign({
        id: userId,
        exp: exp.getTime() / 1000
      }, _config["default"].JWT_SECRET);
    }
  }]);

  return AuthService;
}();

exports["default"] = AuthService;
//# sourceMappingURL=AuthService.js.map