"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _models = require("../database/models/");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var PostService = /*#__PURE__*/function () {
  function PostService() {
    _classCallCheck(this, PostService);
  }

  _createClass(PostService, [{
    key: "createPost",

    /**
     * @description Create new post
     * @param {title,body,photo} 
     */
    value: function () {
      var _createPost = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(title, body, photo, userId) {
        var user;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _models.UserModel.findById(userId);

              case 2:
                user = _context.sent;
                return _context.abrupt("return", _models.PostModel.create({
                  title: title,
                  body: body,
                  photo: photo,
                  postedBy: user.id
                }));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function createPost(_x, _x2, _x3, _x4) {
        return _createPost.apply(this, arguments);
      }

      return createPost;
    }()
    /**
     * @description Sub Posts
     * @param {object} params 
     */

  }, {
    key: "getSubPosts",
    value: function () {
      var _getSubPosts = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref) {
        var _ref$params, params;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _ref$params = _ref.params, params = _ref$params === void 0 ? {} : _ref$params;
                return _context2.abrupt("return", _models.PostModel.find({
                  postedBy: {
                    $in: params
                  }
                }).populate("postedBy", "_id name").populate("comments.postedBy", "_id name").populate("likes", "_id name").sort("-createdAt"));

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getSubPosts(_x5) {
        return _getSubPosts.apply(this, arguments);
      }

      return getSubPosts;
    }()
  }, {
    key: "getUsersPosts",
    value: function () {
      var _getUsersPosts = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(id) {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", _models.PostModel.find({
                  postedBy: id
                }).populate("postedBy", "_id name"));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getUsersPosts(_x6) {
        return _getUsersPosts.apply(this, arguments);
      }

      return getUsersPosts;
    }()
    /**
    * @description All Posts
    * @param {object} 
    */

  }, {
    key: "getAllPosts",
    value: function () {
      var _getAllPosts = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                return _context4.abrupt("return", _models.PostModel.find({}).populate("postedBy", "_id name photo").populate("comments.postedBy", "_id name, text").populate("likes", "_id name photo").sort("-createdAt"));

              case 1:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function getAllPosts() {
        return _getAllPosts.apply(this, arguments);
      }

      return getAllPosts;
    }()
  }, {
    key: "getSinglePost",
    value: function () {
      var _getSinglePost = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(id) {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                return _context5.abrupt("return", _models.PostModel.findById(id).populate("postedBy", "_id name photo").populate("comments.postedBy", "_id name photo text").populate("likes", "_id name"));

              case 1:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function getSinglePost(_x7) {
        return _getSinglePost.apply(this, arguments);
      }

      return getSinglePost;
    }()
  }, {
    key: "commentPost",
    value: function () {
      var _commentPost = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(postId, text, id) {
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                return _context6.abrupt("return", _models.PostModel.findByIdAndUpdate(postId, {
                  $push: {
                    comments: {
                      text: text,
                      postedBy: id
                    }
                  }
                }, {
                  "new": true
                }).populate("comments.postedBy", "_id name photo").populate("postedBy", "_id name photo"));

              case 1:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));

      function commentPost(_x8, _x9, _x10) {
        return _commentPost.apply(this, arguments);
      }

      return commentPost;
    }()
    /**
     * @description likePost
     * @param {string} postId 
     * @param {string} userId 
     */

  }, {
    key: "likePost",
    value: function () {
      var _likePost = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(postId, userId) {
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                return _context7.abrupt("return", _models.PostModel.findByIdAndUpdate(postId, {
                  $push: {
                    likes: userId
                  }
                }, {
                  "new": true
                }).populate("postedBy", "_id name").populate("comments.postedBy", "_id name photo"));

              case 1:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }));

      function likePost(_x11, _x12) {
        return _likePost.apply(this, arguments);
      }

      return likePost;
    }()
    /**
     * @description unlikePost
     * @param {object} params 
     */

  }, {
    key: "unlikePost",
    value: function () {
      var _unlikePost = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(postId, userId) {
        return regeneratorRuntime.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                return _context8.abrupt("return", _models.PostModel.findByIdAndUpdate(postId, {
                  $pull: {
                    likes: userId
                  }
                }, {
                  "new": true
                }).populate("postedBy", "_id name").populate("comments.postedBy", "_id name photo"));

              case 1:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }));

      function unlikePost(_x13, _x14) {
        return _unlikePost.apply(this, arguments);
      }

      return unlikePost;
    }()
    /**
     * @description Sub Posts
     * @param {object} params 
     */

  }, {
    key: "deletePost",
    value: function () {
      var _deletePost = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(_ref2) {
        var postId, user, findPost;
        return regeneratorRuntime.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                postId = _ref2.postId, user = _ref2.user;
                _context9.next = 3;
                return Post.findOne({
                  _id: postId
                }).populate("postedBy", "_id");

              case 3:
                findPost = _context9.sent;
                return _context9.abrupt("return", findPost.exec(function (err, post) {
                  if (err || !post) return err;

                  if (post.postedBy._id.toString() === user._id.toString()) {
                    post.remove().then(function (result) {
                      return result._id;
                    })["catch"](function (err) {
                      return console.log(err);
                    });
                  }
                }));

              case 5:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }));

      function deletePost(_x15) {
        return _deletePost.apply(this, arguments);
      }

      return deletePost;
    }()
  }]);

  return PostService;
}();

exports["default"] = PostService;
//# sourceMappingURL=PostService.js.map