"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var StatusService = /*#__PURE__*/function () {
  function StatusService() {
    _classCallCheck(this, StatusService);
  }

  _createClass(StatusService, null, [{
    key: "buildResponse",

    /**
     * @desc Построение ответа сервера
     * @return {Object}
     **/
    value: function buildResponse(success, payload) {
      var status = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
      var error = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : undefined;
      return {
        error: error,
        status: status,
        success: !!success,
        data: payload
      };
    }
    /**
     * @desc
     * @param {String} message
     * @param {Number} status
     * @return {Object}
     **/

  }, {
    key: "buildError",
    value: function buildError(message) {
      var status = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
      return StatusService.buildResponse(false, undefined, status, message);
    }
  }]);

  return StatusService;
}();

exports["default"] = StatusService;
//# sourceMappingURL=StatusService.js.map