"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _models = require("../database/models/");

var _PostService = _interopRequireDefault(require("./PostService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var UserService = /*#__PURE__*/function () {
  function UserService() {
    _classCallCheck(this, UserService);
  }

  _createClass(UserService, [{
    key: "getSinglePost",

    /**
     * @description Single User
     * @param {string} id
     */
    value: function () {
      var _getSinglePost = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(id) {
        var user;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _models.UserModel.findById(id).populate("following", "id name photo").populate("followers", "id name photo");

              case 2:
                user = _context.sent;
                _context.t0 = user;
                _context.next = 6;
                return new _PostService["default"]().getUsersPosts(id);

              case 6:
                _context.t1 = _context.sent;
                return _context.abrupt("return", {
                  user: _context.t0,
                  posts: _context.t1
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getSinglePost(_x) {
        return _getSinglePost.apply(this, arguments);
      }

      return getSinglePost;
    }()
  }, {
    key: "follow",
    value: function () {
      var _follow = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(followId, userID) {
        var follow, following;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _models.UserModel.findByIdAndUpdate(followId, {
                  $push: {
                    followers: userID
                  }
                }, {
                  "new": true
                });

              case 2:
                follow = _context2.sent;
                _context2.next = 5;
                return _models.UserModel.findByIdAndUpdate(userID, {
                  $push: {
                    following: followId
                  }
                }, {
                  "new": true
                });

              case 5:
                following = _context2.sent;
                return _context2.abrupt("return", {
                  follow: follow,
                  following: following
                });

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function follow(_x2, _x3) {
        return _follow.apply(this, arguments);
      }

      return follow;
    }()
  }, {
    key: "unFollow",
    value: function () {
      var _unFollow = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(followId, userID) {
        var follow, following;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return _models.UserModel.findByIdAndUpdate(followId, {
                  $pull: {
                    followers: userID
                  }
                }, {
                  "new": true
                });

              case 2:
                follow = _context3.sent;
                _context3.next = 5;
                return _models.UserModel.findByIdAndUpdate(userID, {
                  $pull: {
                    following: followId
                  }
                }, {
                  "new": true
                });

              case 5:
                following = _context3.sent;
                return _context3.abrupt("return", {
                  follow: follow,
                  following: following
                });

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function unFollow(_x4, _x5) {
        return _unFollow.apply(this, arguments);
      }

      return unFollow;
    }()
  }, {
    key: "getSuggestion",
    value: function () {
      var _getSuggestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(userID) {
        var _yield$UserModel$find, id, following, allUsers, followArr;

        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return _models.UserModel.findById(userID).populate("following", "id");

              case 2:
                _yield$UserModel$find = _context4.sent;
                id = _yield$UserModel$find.id;
                following = _yield$UserModel$find.following;
                _context4.next = 7;
                return _models.UserModel.find({});

              case 7:
                allUsers = _context4.sent;
                followArr = following.map(function (item) {
                  return item.id;
                });
                return _context4.abrupt("return", allUsers.filter(function (user) {
                  return followArr.indexOf(user.id) === -1 && user.id !== id;
                }));

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function getSuggestion(_x6) {
        return _getSuggestion.apply(this, arguments);
      }

      return getSuggestion;
    }()
  }]);

  return UserService;
}();

exports["default"] = UserService;
//# sourceMappingURL=UserService.js.map