"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "PostMapper", {
  enumerable: true,
  get: function get() {
    return _PostMapper["default"];
  }
});
Object.defineProperty(exports, "UserMapper", {
  enumerable: true,
  get: function get() {
    return _UserMapper["default"];
  }
});

var _PostMapper = _interopRequireDefault(require("./PostMapper"));

var _UserMapper = _interopRequireDefault(require("./UserMapper"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
//# sourceMappingURL=index.js.map