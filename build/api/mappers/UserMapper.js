"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var UserMapper = /*#__PURE__*/function () {
  function UserMapper() {
    _classCallCheck(this, UserMapper);
  }

  _createClass(UserMapper, null, [{
    key: "createdUser",
    value: function createdUser(user) {
      return {
        id: user._id,
        name: user.name,
        email: user.email,
        photo: user.photo,
        firstName: user.firstName,
        lastName: user.lastName
      };
    }
  }, {
    key: "loginUser",
    value: function loginUser(user) {
      return {
        id: user._id,
        name: user.name,
        email: user.email,
        photo: user.photo,
        firstName: user.firstName,
        lastName: user.lastName,
        followers: user.followers,
        following: user.following
      };
    }
  }]);

  return UserMapper;
}();

exports["default"] = UserMapper;
//# sourceMappingURL=UserMapper.js.map