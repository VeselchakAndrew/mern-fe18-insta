"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.startServer = startServer;

require("regenerator-runtime/runtime.js");

var _config = _interopRequireDefault(require("./config"));

var _loaders = _interopRequireDefault(require("./loaders"));

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var PORT = process.env.PORT || 5000;
/**
 * @desc Function that starts the server
 **/

function startServer() {
  return _startServer.apply(this, arguments);
}

function _startServer() {
  _startServer = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var app;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            app = (0, _express["default"])(); // Load an application

            _context.next = 3;
            return (0, _loaders["default"])({
              app: app,
              config: _config["default"]
            });

          case 3:
            app.listen(PORT, function (err) {
              if (err) {
                console.log("Error has just happened!");
                process.exit(1);
                return;
              }

              console.log("\n        ################################################\n        \uD83D\uDEE1\uFE0F Server listening on port: ".concat(PORT, " and version: ").concat(_config["default"].SERVICE_PREFIX, "/v").concat(_config["default"].VERSION, "\n        ################################################"));
            });

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _startServer.apply(this, arguments);
}

startServer();
//# sourceMappingURL=index.js.map